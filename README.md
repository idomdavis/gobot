# gobot

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/gobot/main?style=plastic)](https://bitbucket.org/idomdavis/gobot/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/gobot?style=plastic)](https://bitbucket.org/idomdavis/gobot/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/gobot?style=plastic)](https://bitbucket.org/idomdavis/gobot/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/gobot)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

A Bot written in Go.

# Coverage Alias

```
alias coverage="go test -coverprofile /tmp/c.out; uncover /tmp/c.out; rm /tmp/c.out;"
```
