package main

import (
	"fmt"
	"os"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/listener"
	"bitbucket.org/idomdavis/gobot/server"
	"bitbucket.org/idomdavis/gobot/store"
	log "github.com/sirupsen/logrus"
)

func main() {
	startup()

	settings := configure()
	backingStore, err := store.Get(settings)

	if err != nil {
		log.WithError(err).Fatal("Failed to get backing store")
	}

	a := []actions.Handler{
		&actions.Ping{BaseHandler: actions.NewBaseHandler("Ping")},
		&actions.Status{
			Settings:    settings,
			BaseHandler: actions.NewBaseHandler("Status"),
		},
		&actions.Moar{
			BackingStore: backingStore,
			BaseHandler:  actions.NewBaseHandler("Moar"),
		},
		&actions.Shutdown{
			BaseHandler: actions.NewBaseHandler("Shutdown"),
		},
	}

	for _, e := range a {
		if err = e.Init(); err != nil {
			log.WithError(err).Fatal("Failed to initialise actions")
		}
	}

	handler := listener.Dispatcher{
		Settings: settings,
		Actions:  a,
	}

	if settings.Sender, err = listener.Start(handler, settings); err != nil {
		log.WithError(err).Fatal("Failed to start chat client")
	}

	log.Info("Bot is running...")

	err = server.Listen(settings.Server.Port, server.Router(server.API, settings))

	if err != nil {
		fmt.Println("Server exited")
	}
}

func startup() {
	log.SetFormatter(&log.TextFormatter{
		DisableLevelTruncation: true,
		FullTimestamp:          true,
		PadLevelText:           true,
	})

	log.WithFields(log.Fields{
		"Build Version":   config.BuildVersion,
		"Build Hash":      config.BuildHash,
		"Build Timestamp": config.BuildTimestamp,
	}).Info("Build Info")
}

func configure() config.Settings {
	settings, err := config.Configure()

	if err != nil {
		log.WithError(err).Error("Failed to configure bot")
		config.Usage()
		os.Exit(-1)
	}

	config.Report(settings)

	return settings
}
