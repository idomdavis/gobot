package listener

import (
	"strings"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/message"
)

// A Dispatcher dispatches incoming messages to each of the Actions which can
// handle them and reply via the Author if needed.
type Dispatcher struct {
	config.Settings

	Actions []actions.Handler
}

// Dispatch an incoming Message to the registered Actions allowing them to
// respond if relevant on the Sender.
func (d Dispatcher) Dispatch(m message.Inbound, s message.Sender) {
	if m.Author.Bot {
		return
	}

	if !strings.HasPrefix(m.Content, d.Settings.Discord.Prefix) {
		return
	}

	m.Content = strings.TrimPrefix(m.Content, d.Settings.Discord.Prefix)

	for _, action := range d.Actions {
		action.Handle(m, s)
	}
}
