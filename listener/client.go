package listener

import (
	"errors"
	"fmt"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/message"
)

// ErrInvalidClientConfig is returned if the bot is started with a client config
// name it doesn't recognise.
var ErrInvalidClientConfig = errors.New("invalid client config")

// ErrConnectionFailure is returned if there was an error connecting to the
// server for any reason.
var ErrConnectionFailure = errors.New("failed to connect to server")

// Start the client using the Dispatcher to handle incoming messages.
func Start(dispatcher Dispatcher, settings config.Settings) (message.Sender, error) {
	var (
		err    error
		sender message.Sender
	)

	switch settings.Server.ChatClientConfig {
	case "discord":
		sender, err = DiscordClient(dispatcher, settings)
	case "console":
		ConsoleClient(dispatcher, settings)
	default:
		err = fmt.Errorf("%w: %s", ErrInvalidClientConfig,
			settings.Server.StoreConfig)
	}

	return sender, err
}
