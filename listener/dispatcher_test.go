package listener_test

import (
	"fmt"
	"testing"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/listener"
	"bitbucket.org/idomdavis/gobot/message"
)

func ExampleDispatcher_Dispatch() {
	d := listener.Dispatcher{
		Actions:  []actions.Handler{&actions.Echo{}},
		Settings: config.Settings{Discord: config.Discord{Prefix: "!"}},
	}

	c := &MockClient{
		Dispatcher: d,
	}

	c.Receive(message.Inbound{
		Channel: "chan",
		Content: d.Settings.Discord.Prefix + "Some amusing message",
		Author: message.User{
			ID:  "sender",
			Bot: false,
		},
	})

	if len(c.Messages) == 1 {
		fmt.Println(c.Messages[0].Message)
	}

	// Output:
	// [chan] sender: Some amusing message
}

func TestDiscord_Handle(t *testing.T) {
	t.Run("Bots should be ignored", func(t *testing.T) {
		d := listener.Dispatcher{
			Actions:  []actions.Handler{&actions.Echo{}},
			Settings: config.Settings{Discord: config.Discord{Prefix: "!"}},
		}

		c := &MockClient{Dispatcher: d}

		c.Receive(message.Inbound{
			Channel: "chan",
			Content: d.Settings.Discord.Prefix + "Some amusing message",
			Author: message.User{
				ID:  "sender",
				Bot: true,
			},
		})

		if len(c.Messages) != 0 {
			t.Errorf("Bots are not being ignored. Expected no messages, got: %d",
				len(c.Messages))
		}
	})

	t.Run("The prefix should be required", func(t *testing.T) {
		d := listener.Dispatcher{
			Actions:  []actions.Handler{&actions.Echo{}},
			Settings: config.Settings{Discord: config.Discord{Prefix: "!"}},
		}

		c := &MockClient{Dispatcher: d}

		c.Receive(message.Inbound{
			Content: "Some amusing message",
			Author:  message.User{ID: "sender"},
		})

		if len(c.Messages) != 0 {
			t.Errorf("Messages without command prefix are not being ignored. "+
				"Expected no messages, got: %d", len(c.Messages))
		}
	})

	t.Run("Actions are not mandatory", func(t *testing.T) {
		d := listener.Dispatcher{
			Actions:  []actions.Handler{},
			Settings: config.Settings{Discord: config.Discord{Prefix: "!"}},
		}

		c := &MockClient{Dispatcher: d}

		c.Receive(message.Inbound{
			Content: d.Settings.Discord.Prefix + "Some amusing message",
			Author:  message.User{ID: "sender"},
		})

		if len(c.Messages) != 0 {
			t.Errorf("An empty actions list is not being handled properly. "+
				"Expected no messages, got: %d", len(c.Messages))
		}
	})

	t.Run("Failure to send it handled gracefully", func(t *testing.T) {
		d := listener.Dispatcher{
			Actions:  []actions.Handler{&actions.Echo{}},
			Settings: config.Settings{Discord: config.Discord{Prefix: "!"}},
		}

		c := &MockClient{Dispatcher: d}

		c.Receive(message.Inbound{
			Content: d.Settings.Discord.Prefix + MockError,
			Author:  message.User{ID: "sender"},
		})

		switch {
		case len(c.Messages) != 1:
			t.Errorf("Unexpected number of messages: %d", len(c.Messages))
		case c.Messages[0].Message != "[] sender: "+MockError:
			t.Errorf("Unexpected message: %s", c.Messages[0].Message)
		}
	})
}
