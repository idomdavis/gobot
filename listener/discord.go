package listener

import (
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/message"
	"github.com/bwmarrin/discordgo"
	log "github.com/sirupsen/logrus"
)

// Discord listener. The set of Actions are used to handle the messages.
type Discord struct {
	Dispatcher
}

// ErrNoDiscordToken is returned if there is no Discord token set in the config.
var ErrNoDiscordToken = errors.New("no discord token set")

// Connect to the Discord server using the given token. The Actions set on the
// Discord type will be used to handle the message.
func (d *Discord) Connect(token string) (*discordgo.Session, error) {
	con, err := discordgo.New("Bot " + token)

	if err != nil {
		return nil, fmt.Errorf("failed to create new bot: %w", err)
	}

	con.AddHandler(d.handler)

	con.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages)

	err = con.Open()

	if err != nil {
		return nil, fmt.Errorf("failed to connect to discord: %w", err)
	}

	return con, nil
}

func (d *Discord) handler(s *discordgo.Session, m *discordgo.MessageCreate) {
	sender := DiscordSender{session: s}
	msg := message.Inbound{
		Channel: m.ChannelID,
		Content: m.Content,
		Author: message.User{
			ID:  m.Author.ID,
			Bot: m.Author.Bot,
		},
	}

	d.Dispatch(msg, sender)
}

// DiscordSender sends messages to Discord.
type DiscordSender struct {
	session *discordgo.Session
}

// Send a message to Discord.
func (d DiscordSender) Send(msg message.Outbound) error {
	m := discordgo.MessageSend{Content: msg.Message}

	if strings.TrimSpace(msg.Message) == "" {
		m.Content = "<this space intentionally[?] left blank>"
	}

	embed := &discordgo.MessageEmbed{
		URL:         msg.Embed.URL,
		Title:       msg.Embed.Title,
		Description: msg.Embed.Description,
	}

	if msg.Embed.Image != "" {
		embed.Image = &discordgo.MessageEmbedImage{URL: msg.Embed.Image}
	}

	if msg.Embed.Video != "" {
		embed.Video = &discordgo.MessageEmbedVideo{URL: msg.Embed.Video}
	}

	embed.Fields = make([]*discordgo.MessageEmbedField,
		len(msg.Embed.Fields))

	for i, f := range msg.Embed.Fields {
		embed.Fields[i] = &discordgo.MessageEmbedField{
			Name:  f.Name,
			Value: f.Value,
		}
	}

	if !embedEmpty(embed) {
		m.Embed = embed
	}

	_, err := d.session.ChannelMessageSendComplex(msg.Channel, &m)

	if err != nil {
		return fmt.Errorf("failed to send message to Discord: %w", err)
	}

	return nil
}

// SendString builds an Outbound message and sends it.
func (d DiscordSender) SendString(channel, msg string) error {
	return d.Send(message.Outbound{Channel: channel, Message: msg})
}

// DiscordClient creates a new connection to Discord using the given Dispatcher
// and Settings. If the connection fails an error is returned. If there is no
// Discord Token set then the program will exit.
func DiscordClient(dispatcher Dispatcher, settings config.Settings) (message.Sender, error) {
	if settings.Discord.Token == "" {
		return nil, ErrNoDiscordToken
	}

	discord := &Discord{Dispatcher: dispatcher}

	session, err := discord.Connect(settings.Discord.Token)

	if err != nil {
		return nil, ErrConnectionFailure
	}

	if err = actions.Avatar(settings.Discord.Avatar, session); err != nil {
		log.WithFields(log.Fields{
			"error":  err,
			"avatar": settings.Discord.Avatar,
		}).Warn("Failed to set avatar")
	}

	return DiscordSender{session: session}, nil
}

func embedEmpty(embed *discordgo.MessageEmbed) bool {
	switch {
	case strings.TrimSpace(embed.Title) != "":
	case strings.TrimSpace(embed.Description) != "":
	case strings.TrimSpace(embed.URL) != "":
	case embed.Image != nil:
	case embed.Video != nil:
	case len(embed.Fields) > 0:
	default:
		return true
	}

	return false
}
