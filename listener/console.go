package listener

import (
	"bufio"
	"fmt"
	"os"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/message"
)

// Console client.
type Console struct {
	Dispatcher
}

// ConsoleClient starts a new client listening on the console.
func ConsoleClient(dispatcher Dispatcher, _ config.Settings) message.Sender {
	s := Console{Dispatcher: dispatcher}

	go s.listen()

	return s
}

// Send will output the message component of the Outbound to STDOUT.
func (c Console) Send(message message.Outbound) error {
	fmt.Println(message.Message)

	return nil
}

// SendString will build an Outbound and Send that.
func (c Console) SendString(channel, msg string) error {
	return c.Send(message.Outbound{Channel: channel, Message: msg})
}

func (c Console) listen() {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("Enter message: ")

		text, _ := reader.ReadString('\n')

		c.Dispatch(message.Inbound{
			Channel: "stdin",
			Content: text,
			Author:  message.User{ID: "console"},
		}, c)
	}
}
