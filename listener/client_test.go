package listener_test

import (
	"errors"

	"bitbucket.org/idomdavis/gobot/listener"
	"bitbucket.org/idomdavis/gobot/message"
)

// MockClient is used to aid testing providing a fully controllable and
// interrogate-able client.
type MockClient struct {
	listener.Dispatcher

	Messages []message.Outbound
}

// MockError is used to have Send/SendString return an error.
var MockError = "Error"

// ErrMockError is returned if the message being handled is MockError .
var ErrMockError = errors.New("mock error requested")

// Receive a test message to dispatch.
func (m *MockClient) Receive(msg message.Inbound) {
	m.Dispatcher.Dispatch(msg, m)
}

// Send appends the message to the set of Messages, returning ErrMockError if
// the message string is MockError.
func (m *MockClient) Send(msg message.Outbound) error {
	m.Messages = append(m.Messages, msg)

	if msg.Message == MockError {
		return ErrMockError
	}

	return nil
}

// SendString will build an Outbound and Send that.
func (m *MockClient) SendString(channel, msg string) error {
	return m.Send(message.Outbound{
		Channel: channel,
		Message: msg,
	})
}
