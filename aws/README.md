# AWS Stuff

`iam-policy.json` is a template inline policy for your IAM user. `<LOC>` is the
AWS region you want to run in (e.g. `eu-west-2`), `<ACCT>` is you account number
(can get it from the AWS console), `<CLUSTER>` is the `ecs` cluster name,
`SERVICE` is the `ecs` service name, and `<NAME>` is whatever you've called the
Docker image.

`s3-bucket-policy.json` is a template for an S3 bucket used to store config.
`<VPC>` is the ID of the VPC you want to give access too, and `<BUCKET>` is the
name of the bucket/directories you want to give access to.
