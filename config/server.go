package config

import (
	"bitbucket.org/idomdavis/goconfigure"
	"github.com/sirupsen/logrus"
)

// Server options.
type Server struct {
	Port             int
	StoreConfig      string
	ChatClientConfig string
}

// Configure the Server options.
func (s *Server) Configure(opts *goconfigure.Options) {
	opts.Add(&goconfigure.Option{
		Pointer:     &s.Port,
		Description: "The HTTP Server port",
		LongFlag:    "port",
		ConfigKey:   "port",
		Default:     1337,
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &s.StoreConfig,
		Description: "Name of the store configuration (s3 or memory)",
		LongFlag:    "store",
		ConfigKey:   "store",
		Default:     "s3",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &s.ChatClientConfig,
		Description: "Name of the chat client configuration (discord or console",
		LongFlag:    "client",
		ConfigKey:   "client",
		Default:     "discord",
	})
}

// Report the configuration for Server.
func (s *Server) Report() {
	logrus.WithFields(logrus.Fields{
		"Server HTTP Port":   s.Port,
		"Store Config":       s.StoreConfig,
		"Chat Client Config": s.ChatClientConfig,
	}).Info("Server Settings")
}
