package config

import (
	"time"

	"bitbucket.org/idomdavis/goconfigure"
	"github.com/sirupsen/logrus"
)

// Security options.
type Security struct {
	// Secret used to encode the JWT tokens
	Secret string

	// SessionTTL is the length of time a session will be valid for before
	// it requires re-authentication.
	SessionTTL time.Duration

	// Timebox is the minimum time it will take for a login attempt to
	// return.
	Timebox time.Duration

	Limiters
}

// Configure the security options.
//nolint:gomnd
func (s *Security) Configure(opts *goconfigure.Options) {
	opts.Add(goconfigure.NewOption(&s.Secret, "", stub, "secret",
		"Secret used to secure the JWT tokens"))
	opts.Add(goconfigure.NewOption(&s.SessionTTL, time.Hour*12, stub,
		"session-ttl", "TTL for sessions"))
	opts.Add(goconfigure.NewOption(&s.Timebox, time.Millisecond*500, stub,
		"login-timebox", "Minimum time it will take for a login attempt to return"))

	s.Limiters.Configure(opts)
}

// Report the configuration for the security settings.
func (s *Security) Report() {
	logrus.WithFields(logrus.Fields{
		"Secret":      goconfigure.Sanitise(s.Secret, set, unset),
		"Session TTL": goconfigure.Sanitise(s.SessionTTL, s.SessionTTL, unset),
		"Timebox":     goconfigure.Sanitise(s.Timebox, s.Timebox, unset),
	}).Info("Security settings")

	s.Limiters.Report()
}
