package config

import (
	"fmt"

	"bitbucket.org/idomdavis/gobot/message"
	"bitbucket.org/idomdavis/goconfigure"
)

// Settings available to the bot.
type Settings struct {
	Bearer string

	Server   Server
	Discord  Discord
	Security Security
	AWS      AWS

	message.Sender

	config string
}

var opts = goconfigure.NewOptions()

const (
	set   = "SET"
	unset = "UNSET"

	stub = "GOBOT"
)

// Configure the bot from the defined configuration settings.
func Configure() (Settings, error) {
	var settings Settings

	opts.Add(&goconfigure.Option{
		Pointer:     &settings.Bearer,
		Description: "Super secret never to be shown string",
		ConfigKey:   "bearer",
	})

	settings.Server.Configure(opts)
	settings.Discord.Configure(opts)
	settings.Security.Configure(opts)
	settings.AWS.Configure(opts)

	opt := goconfigure.NewFlag(&settings.config, 'c',
		"The path to the configuration file")

	opts.Add(opt)
	opts.LoadUsing(opt)

	if err := opts.Parse(); err != nil {
		return settings, fmt.Errorf("failed to configure bot: %w", err)
	}

	return settings, nil
}

// Usage prints the usage string.
func Usage() {
	opts.Usage()
}

// Report the settings to the logfile.
func Report(settings Settings) {
	settings.Server.Report()
	settings.Discord.Report()
	settings.Security.Report()
	settings.AWS.Report()
}
