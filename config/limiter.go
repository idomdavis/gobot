package config

import (
	"fmt"
	"time"

	"bitbucket.org/idomdavis/goconfigure"
	"github.com/sirupsen/logrus"
)

// Limiter configuration.
type Limiter struct {
	// Depth of the limiter (i.e. number of distinct items to hold)
	Depth int

	// Limit at which the limiter will trip (i.e. the number of time something
	// needs to be seen before the limiter trips)
	Limit int

	// TTL for the items in the limiter
	TTL time.Duration
}

// Configure the limiter.
func (l *Limiter) Configure(opts *goconfigure.Options, name string, limit int) {
	opts.Add(goconfigure.NewOption(&l.Depth, 1000, stub,
		fmt.Sprintf("%s-limiter-depth", name),
		"Depth of the limiter (i.e. number of distinct items to hold)"))
	opts.Add(goconfigure.NewOption(&l.Limit, limit, stub,
		fmt.Sprintf("%s-limiter-limit", name),
		"Number of times something needs to be seen before the limiter trips"))
	opts.Add(goconfigure.NewOption(&l.TTL, time.Second, stub,
		fmt.Sprintf("%s-limiter-ttl", name),
		"Time-to-live for items in the limiter"))
}

// Report the configuration for the Limiter.
func (l *Limiter) Report(name string) {
	logrus.WithFields(logrus.Fields{
		"Type":  name,
		"Depth": goconfigure.Sanitise(l.Depth, l.Depth, unset),
		"Limit": goconfigure.Sanitise(l.Limit, l.Limit, unset),
		"TTL":   goconfigure.Sanitise(l.TTL, l.TTL, unset),
	}).Info("Limiter settings")
}
