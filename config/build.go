package config

// BuildTimestamp is set by the Makefile and is the time the binary was built.
var BuildTimestamp = unset

// BuildHash is set by the Makefile and is the current git hash when the binary
// was built. Note if there are uncommitted changes in repository then the
// hash may not be accurate.
var BuildHash = unset

// BuildVersion is set by the Makefile and is taken from the git branch, or
// git tag.
var BuildVersion = unset
