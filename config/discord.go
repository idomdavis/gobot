package config

import (
	"bitbucket.org/idomdavis/goconfigure"
	"github.com/sirupsen/logrus"
)

// Discord options.
type Discord struct {
	Name  string
	Token string

	Bearer string
	Avatar string
	Prefix string
}

// Configure the Discord options.
func (d *Discord) Configure(opts *goconfigure.Options) {
	opts.Add(&goconfigure.Option{
		Pointer:     &d.Name,
		Description: "The bot name",
		LongFlag:    "name",
		ConfigKey:   "name",
		Default:     "gobot",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &d.Token,
		Description: "Discord token used to connect the bot",
		ConfigKey:   "token",
		EnvVar:      "GOBOT_DISCORD_TOKEN",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &d.Bearer,
		Description: "Super secret never to be shown string",
		ConfigKey:   "bearer",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &d.Avatar,
		Description: "Path to the avatar",
		ConfigKey:   "avatar",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &d.Prefix,
		Description: "Prefix string used by the bot",
		ConfigKey:   "prefix",
		Default:     "!",
	})
}

// Report the configuration for Discord.
func (d *Discord) Report() {
	logrus.WithFields(logrus.Fields{
		"Bot Name":      d.Name,
		"Bot Prefix":    d.Prefix,
		"Bot Avatar":    d.Avatar,
		"Discord Token": goconfigure.Sanitise(d.Token, set, unset),
	}).Info("Discord Settings")
}
