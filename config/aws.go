package config

import (
	"bitbucket.org/idomdavis/goconfigure"
	"github.com/sirupsen/logrus"
)

// AWS options.
type AWS struct {
	Region    string
	Bucket    string
	AccessKey string
	SecretKey string
}

// Configure the AWS options.
func (a *AWS) Configure(opts *goconfigure.Options) {
	opts.Add(&goconfigure.Option{
		Pointer:     &a.Region,
		Description: "AWS Region we're connecting to for S3 storage",
		LongFlag:    "region",
		ConfigKey:   "aws-region",
		EnvVar:      "AWS_REGION",
		Default:     "eu-west-2",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &a.Bucket,
		Description: "AWS Bucket we're connecting to for S3 storage",
		LongFlag:    "bucket",
		ConfigKey:   "aws-bucket",
		EnvVar:      "AWS_BUCKET",
		Default:     "",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &a.AccessKey,
		Description: "AWS Access Key",
		ConfigKey:   "aws-access-key",
		EnvVar:      "AWS_SECRET_KEY_ID",
	})

	opts.Add(&goconfigure.Option{
		Pointer:     &a.SecretKey,
		Description: "AWS Secret Key",
		ConfigKey:   "aws-secret-key",
		EnvVar:      "AWS_ACCESS_KEY_ID",
	})
}

// Report the configuration for AWS.
func (a *AWS) Report() {
	logrus.WithFields(logrus.Fields{
		"Region":         a.Region,
		"Bucket":         a.Bucket,
		"AWS Access Key": goconfigure.Sanitise(a.AccessKey, set, unset),
		"AWS Secret Key": goconfigure.Sanitise(a.SecretKey, set, unset),
	}).Info("AWS Settings")
}
