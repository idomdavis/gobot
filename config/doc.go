// Package config handles configuration of the bot and is also a store for
// global variables such as version, etc which are set on building via the
// Makefile.
package config
