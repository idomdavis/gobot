package config

import (
	"bitbucket.org/idomdavis/goconfigure"
)

const (
	source = "source"
	user   = "user"
)

// Limiters in use by the system.
type Limiters struct {
	// Source limiter rate limits connections from the same source.
	Source Limiter

	// User limiter rate limits connections from the same user.
	User Limiter
}

// Configure the limiter.
func (l *Limiters) Configure(opts *goconfigure.Options) {
	l.Source.Configure(opts, source, 100)
	l.User.Configure(opts, user, 5)
}

// Report the configuration for the Limiter.
func (l *Limiters) Report() {
	l.Source.Report(source)
	l.Source.Report(user)
}
