package message

// A Sender is able to send a message to a chat platform.
type Sender interface {
	// Send the Outbound message. If the message fails to send return an
	// error.
	Send(message Outbound) error

	// SendString sends the message as a basic text message to the given
	// channel. If the message fails to send return an error.
	SendString(channel, message string) error
}

// MemorySender just stores messages in an internal list.
type MemorySender struct {
	Messages []Outbound
}

// Send the Outbound message to the internal memory store.
func (m *MemorySender) Send(message Outbound) error {
	m.Messages = append(m.Messages, message)

	return nil
}

// SendString wraps the message in an Outbound message type and calls Send.
func (m *MemorySender) SendString(channel, message string) error {
	return m.Send(Outbound{Channel: channel, Message: message})
}
