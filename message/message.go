package message

// Inbound received from the chat platform.
type Inbound struct {
	Channel string
	Content string
	Author  User
}

// Outbound sent to the chat platform.
type Outbound struct {
	Channel string
	Message string
	Embed   struct {
		URL         string
		Title       string
		Description string
		Colour      string
		Image       string
		Video       string
		Fields      []struct {
			Name  string
			Value string
		}
	}
}
