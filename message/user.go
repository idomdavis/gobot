package message

// User on the chat platform.
type User struct {
	ID  string
	Bot bool
}
