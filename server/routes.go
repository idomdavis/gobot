package server

import (
	"net/http"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/server/endpoint"
)

// Handler types can generate a HandlerFunc for the given settings.
type Handler func(settings config.Settings) http.HandlerFunc

// Handlers defines a set of Gorilla URL paths and their Handler.
type Handlers map[string]Handler

// Routes allows the API to be defined.
type Routes struct {
	Secure   map[string]Handlers
	Insecure map[string]Handlers
}

// API for the bot HTTP endpoints.
var API = Routes{
	Secure: map[string]Handlers{},
	Insecure: map[string]Handlers{
		http.MethodGet: map[string]Handler{
			"/ping":   endpoint.Ping,
			"/coffee": endpoint.Coffee,
		},
		http.MethodPost: map[string]Handler{
			"/send": endpoint.Send,
		},
	},
}
