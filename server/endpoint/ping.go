package endpoint

import (
	"net/http"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/log"
	"bitbucket.org/idomdavis/gohttp/conversation"
)

// Ping simply replies "pong".
func Ping(_ config.Settings) http.HandlerFunc {
	handler := log.Handler("ping")

	return func(w http.ResponseWriter, r *http.Request) {
		handler.Handle(conversation.Reply(w, "pong"))
	}
}
