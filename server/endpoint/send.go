package endpoint

import (
	"net/http"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/log"
	"bitbucket.org/idomdavis/gobot/message"
	"bitbucket.org/idomdavis/gohttp/conversation"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Send sends a message to discord.
func Send(settings config.Settings) http.HandlerFunc {
	var msg message.Outbound

	handler := log.Handler("send")

	return func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Authorization") != settings.Bearer {
			handler.Handle(conversation.Respond(w, http.StatusUnauthorized))
			return
		}

		if err := conversation.Unmarshal(r.Body, &msg); err != nil {
			handler.Handle(conversation.Respond(w, http.StatusBadRequest))
			return
		}

		m := discordgo.MessageSend{
			Content: msg.Message,
			Embed: &discordgo.MessageEmbed{
				URL:         msg.Embed.URL,
				Title:       msg.Embed.Title,
				Description: msg.Embed.Description,
			},
		}

		if msg.Embed.Image != "" {
			m.Embed.Image = &discordgo.MessageEmbedImage{URL: msg.Embed.Image}
		}

		if msg.Embed.Video != "" {
			m.Embed.Video = &discordgo.MessageEmbedVideo{URL: msg.Embed.Video}
		}

		m.Embed.Fields = make([]*discordgo.MessageEmbedField,
			len(msg.Embed.Fields))

		for i, f := range msg.Embed.Fields {
			m.Embed.Fields[i] = &discordgo.MessageEmbedField{
				Name:  f.Name,
				Value: f.Value,
			}
		}

		if err := settings.Sender.Send(msg); err != nil {
			logrus.WithError(err).Error("Failed to forward msg")
			handler.Handle(conversation.Respond(w, http.StatusInternalServerError))

			return
		}

		handler.Handle(conversation.Respond(w, http.StatusOK))
	}
}
