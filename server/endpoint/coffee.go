package endpoint

import (
	"net/http"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/log"
	"bitbucket.org/idomdavis/gohttp/conversation"
)

// Coffee returns 418.
func Coffee(_ config.Settings) http.HandlerFunc {
	handler := log.Handler("coffee")

	return func(w http.ResponseWriter, r *http.Request) {
		handler.Handle(conversation.Respond(w, http.StatusTeapot))
	}
}
