package server

import (
	"net/http"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gohttp/handler"
	"bitbucket.org/idomdavis/gohttp/middleware"
	"bitbucket.org/idomdavis/gohttp/session"
	"github.com/gorilla/mux"
)

// Router builds the http router providing secure and insecure sub routers and
// limiters on both.
func Router(routes Routes, settings config.Settings) http.Handler {
	userLimiter := middleware.RateLimiter{
		ActionFunc: middleware.User,
		Limiter: session.Limiter{
			Size:   settings.Security.User.Depth,
			Limit:  settings.Security.User.Limit,
			Window: settings.Security.User.TTL,
		},
	}
	sourceLimiter := middleware.RateLimiter{
		ActionFunc: middleware.Source,
		Limiter: session.Limiter{
			Size:   settings.Security.Source.Depth,
			Limit:  settings.Security.Source.Limit,
			Window: settings.Security.Source.TTL,
		},
	}

	router := mux.NewRouter()
	router.Use(userLimiter.Middleware)
	router.Use(sourceLimiter.Middleware)

	secure := router.NewRoute().Subrouter()
	secure.Use(middleware.Authenticator{
		Signatory: session.JWTSignatory{
			Secret: settings.Security.Secret,
			TTL:    settings.Security.SessionTTL,
		},
		Deny: handler.Unauthorised{}.Handle,
	}.Authenticate)

	for method, handlers := range routes.Insecure {
		for p, h := range handlers {
			router.HandleFunc(p, h(settings)).Methods(method)
		}
	}

	for method, handlers := range routes.Secure {
		for p, h := range handlers {
			secure.HandleFunc(p, h(settings)).Methods(method)
		}
	}

	return router
}
