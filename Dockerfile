FROM scratch

ADD gobot /
ADD avatar.png /
ADD certs/ca-certificates.crt /etc/ssl/certs/

ENTRYPOINT ["./gobot"]
