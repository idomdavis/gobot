package log

import (
	"bitbucket.org/idomdavis/gohttp/conversation"
	"github.com/sirupsen/logrus"
)

// Handler logs all errors from HTTP conversations.
func Handler(msg string) conversation.Error {
	c := make(chan error)
	handler := conversation.Error{Channel: c}

	go func() {
		for err := range c {
			logrus.WithField("error", err).Errorf(msg)
		}
	}()

	return handler
}
