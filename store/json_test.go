package store_test

import (
	"fmt"

	"bitbucket.org/idomdavis/gobot/store"
)

func ExampleJSONStore_Get() {
	var (
		out  string
		zero int
	)

	s := store.JSONStore{BackingStore: &store.Memory{}}

	_ = s.Set("k", "in")
	_ = s.Get("k", &out)

	if err := s.Get("unset", &zero); err != nil {
		fmt.Println(err)
	}

	fmt.Println(out)
	fmt.Println(zero)

	// Output:
	// in
	// 0
}
