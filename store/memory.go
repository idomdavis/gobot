package store

// Memory store primarily used for testing.
type Memory struct {
	data map[string]string
}

// Get data from the in memory store.
func (m *Memory) Get(key string) (string, error) {
	m.lazyInit()

	return m.data[key], nil
}

// Set data into the in memory store.
func (m *Memory) Set(key, data string) error {
	m.lazyInit()

	m.data[key] = data

	return nil
}

func (m *Memory) lazyInit() {
	if m.data == nil {
		m.data = map[string]string{}
	}
}
