package store

import (
	"encoding/json"
	"fmt"
)

// JSONStore stores JSON documents, converting them to strings for the backing
// store.
type JSONStore struct {
	BackingStore GetSetter
}

// Get data from JSON stored in the backing store, marshalling the JSON into
// the given pointer.
func (j *JSONStore) Get(key string, i interface{}) error {
	if j.BackingStore == nil {
		return ErrNoBackingStore
	}

	if s, err := j.BackingStore.Get(key); err != nil {
		return fmt.Errorf("failed to retrieve JSON from backing store: %w", err)
	} else if s == "" {
		//nolint:ineffassign
		s = `""`
	} else if err = json.Unmarshal([]byte(s), i); err != nil {
		return fmt.Errorf("failed to unmarshal JSON from backing store: %w", err)
	}

	return nil
}

// Set a type in the store marshalling it to JSON for storage.
func (j *JSONStore) Set(key string, i interface{}) error {
	if j.BackingStore == nil {
		return ErrNoBackingStore
	}

	if b, err := json.Marshal(i); err != nil {
		return fmt.Errorf("failed to marshal JSON for store: %w", err)
	} else if err = j.BackingStore.Set(key, string(b)); err != nil {
		return fmt.Errorf("failed to store JSON: %w", err)
	}

	return nil
}
