package aws

import (
	"fmt"
	"strings"

	"bitbucket.org/idomdavis/gobot/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// S3 handles persisting to an S3 bucket.
type S3 struct {
	config.Settings `json:"-"`
}

const suffix = ".json"

// Get data from S3.
func (s S3) Get(key string) (string, error) {
	sess, err := s.session()

	if err != nil {
		return "", fmt.Errorf("failed to get AWS session: %w", err)
	}

	downloader := s3manager.NewDownloader(sess)

	buffer := aws.NewWriteAtBuffer([]byte{})

	_, err = downloader.Download(buffer, &s3.GetObjectInput{
		Bucket: aws.String(s.AWS.Bucket),
		Key:    aws.String(key + suffix),
	})

	if err != nil {
		return "", fmt.Errorf("failed to download s3 data: %w", err)
	}

	return string(buffer.Bytes()), nil
}

// Set a document in S3. The provided data must marshal to JSON.
func (s S3) Set(key, data string) error {
	sess, err := s.session()

	if err != nil {
		return fmt.Errorf("failed to get AWS session: %w", err)
	}

	uploader := s3manager.NewUploader(sess)

	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.AWS.Bucket),
		Key:    aws.String(key + suffix),
		Body:   strings.NewReader(data),
	})

	if err != nil {
		return fmt.Errorf("failed to upload data to S3: %w", err)
	}

	return nil
}

func (s S3) session() (*session.Session, error) {
	c := credentials.NewStaticCredentials(s.AWS.AccessKey, s.AWS.SecretKey, "")

	return session.NewSession(&aws.Config{
		Credentials: c,
		Region:      &s.AWS.Region,
	})
}
