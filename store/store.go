package store

import (
	"errors"
	"fmt"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/store/aws"
)

// Getter types can get data from the backing store.
type Getter interface {
	// Get data from the backing store.
	Get(key string) (string, error)
}

// Setter types can set data into the backing store.
type Setter interface {
	// Set data in the backing store on the given key.
	Set(key, data string) error
}

// GetSetter types can Get and Set from a backing store.
type GetSetter interface {
	Getter
	Setter
}

// ErrNoBackingStore is returned if no backing store is set on the Locking store.
var ErrNoBackingStore = errors.New("no backing store set")

// ErrInvalidStoreConfig is returned if the bot is started with a store config
// name it doesn't recognise.
var ErrInvalidStoreConfig = errors.New("invalid store config")

// Get the backing store to use.
func Get(settings config.Settings) (JSONStore, error) {
	var err error

	jsonStore := JSONStore{}

	switch settings.Server.StoreConfig {
	case "s3":
		jsonStore.BackingStore = &Locking{
			BackingStore: aws.S3{Settings: settings},
		}
	case "memory":
		jsonStore.BackingStore = &Locking{
			BackingStore: &Memory{},
		}
	default:
		err = fmt.Errorf("%w: %s", ErrInvalidStoreConfig,
			settings.Server.StoreConfig)
	}

	return jsonStore, err
}
