package store_test

import (
	"fmt"

	"bitbucket.org/idomdavis/gobot/store"
)

func ExampleMemory_Get() {
	s := store.Memory{}

	_ = s.Set("k", "v")
	v, _ := s.Get("k")

	fmt.Println(v)

	// Output:
	// v
}
