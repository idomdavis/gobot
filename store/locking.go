package store

import (
	"sync"
)

// Locking store single threads access to a store.
type Locking struct {
	mutex sync.Mutex

	BackingStore GetSetter
}

// Get data from the store. The store is locked while being accessed.
func (l *Locking) Get(key string) (string, error) {
	if l.BackingStore == nil {
		return "", ErrNoBackingStore
	}

	l.mutex.Lock()
	defer l.mutex.Unlock()

	return l.BackingStore.Get(key)
}

// Set data on the store. The store is locked while being accessed.
func (l *Locking) Set(key string, data string) error {
	if l.BackingStore == nil {
		return ErrNoBackingStore
	}

	l.mutex.Lock()
	defer l.mutex.Unlock()

	return l.BackingStore.Set(key, data)
}
