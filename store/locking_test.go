package store_test

import (
	"fmt"

	"bitbucket.org/idomdavis/gobot/store"
)

func ExampleLocking_Get() {
	s := store.Locking{BackingStore: &store.Memory{}}

	_ = s.Set("k", "v")

	v, _ := s.Get("k")

	fmt.Println(v)

	// Output:
	// v
}
