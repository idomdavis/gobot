package actions_test

import (
	"strings"
	"testing"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/listener"
	"bitbucket.org/idomdavis/gobot/message"
)

func ExampleStatus_Handle() {
	a := actions.Status{Settings: config.Settings{Discord: config.Discord{Name: "bot"}}}
	_ = a.Init()

	msg := message.Inbound{
		Channel: "chan",
		Content: "status",
		Author:  message.User{ID: "user"},
	}

	s := listener.Console{}

	a.Handle(msg, s)
}

func TestStatus_Handle(t *testing.T) {
	t.Run("Action will output current stauts", func(t *testing.T) {
		a := &actions.Status{
			Settings: config.Settings{Discord: config.Discord{Name: "bot"}}}
		msg := message.Inbound{
			Channel: "chan",
			Content: "status",
			Author:  message.User{ID: "user"},
		}

		s := &message.MemorySender{}

		a.Handle(msg, s)

		expected := "bot: UNSET (Built: UNSET) - Uptime:"

		switch {
		case len(s.Messages) != 1:
			t.Error("Expected a single response from status")
		case !strings.HasPrefix(s.Messages[0].Message, expected):
			t.Errorf("Unexpected response from status: %s",
				s.Messages[0].Message)
		}
	})

	t.Run("Action will ignore irrelevant text", func(t *testing.T) {
		a := &actions.Status{}
		msg := message.Inbound{
			Channel: "chan",
			Content: "let us play pong!",
			Author:  message.User{ID: "user"},
		}

		s := &message.MemorySender{}

		a.Handle(msg, s)

		if len(s.Messages) != 0 {
			t.Errorf("Unexpected response: %s", s.Messages)
		}
	})
}
