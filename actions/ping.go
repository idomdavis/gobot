package actions

import (
	"strings"

	"bitbucket.org/idomdavis/gobot/message"
	"github.com/sirupsen/logrus"
)

// Ping action is a simple "status check" that will response with "Pong" to
// indicate the bot is up and listening. Ping also understands pong and
// responds with an appropriate message.
type Ping struct {
	*BaseHandler
}

// Handle the chat message by replying Ping to Pong and Pong to ping.
func (p *Ping) Handle(inbound message.Inbound, sender message.Sender) {
	var msg string

	switch strings.ToLower(inbound.Content) {
	case "ping":
		msg = "Pong!"
	case "pong":
		msg = "Let us play pong! https://www.youtube.com/watch?v=HTm41_VtxAQ"
	default:
		return
	}

	err := sender.Send(message.Outbound{Channel: inbound.Channel, Message: msg})

	if err != nil {
		logrus.WithError(err).Error("Failed to send ping message")
	}
}
