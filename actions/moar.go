package actions

import (
	"fmt"
	"strings"

	"bitbucket.org/idomdavis/gobot/message"
	"bitbucket.org/idomdavis/gobot/store"
	"github.com/sirupsen/logrus"
)

// Moar just displays information about the bot.
type Moar struct {
	BackingStore store.JSONStore
	Counter      int

	*BaseHandler
}

const moarKey = "moar"

// Handle the chat message by incrementing a counter.
func (m *Moar) Handle(inbound message.Inbound, sender message.Sender) {
	if strings.ToLower(inbound.Content) != "moar" {
		return
	}

	m.Counter++

	msg := fmt.Sprintf("Now at: %d", m.Counter)

	if err := m.BackingStore.Set(moarKey, m.Counter); err != nil {
		logrus.WithError(err).Error("Failed to increment moar counter")

		msg = "Ermahgerd!"
	}

	if err := sender.SendString(inbound.Channel, msg); err != nil {
		logrus.WithError(err).Error("Failed send moar message")
	}
}

// Init the action.
func (m *Moar) Init() error {
	if err := m.BackingStore.Get(moarKey, &m.Counter); err != nil {
		return fmt.Errorf("failed to initialise Moar action: %w", err)
	}

	return nil
}
