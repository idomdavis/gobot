package actions_test

import (
	"fmt"

	"bitbucket.org/idomdavis/gobot/actions"
)

func ExampleNewBaseHandler() {
	var h *actions.BaseHandler

	h = actions.NewBaseHandler("Named")
	fmt.Println(h.ID())

	h = &actions.BaseHandler{}
	fmt.Println(h.ID())

	// Output:
	// Named Action
	// Unnamed Action
}
