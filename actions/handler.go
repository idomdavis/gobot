package actions

import (
	"bitbucket.org/idomdavis/gobot/message"
)

// Handler defines a type that can handle incoming chat messages.
type Handler interface {

	// Handle the message, responding if relevant. Errors should be logged
	// and/or reported to the user via the sender.
	Handle(inbound message.Inbound, sender message.Sender)

	// ID returns the name of the action in order to identify it in logs and
	// errors.
	ID() string

	// Init the handler
	Init() error
}

// BaseHandler provides the basic support elements for a handler.
type BaseHandler struct {
	Name string
}

// NewBaseHandler returns a new, named BaseHandler.
func NewBaseHandler(name string) *BaseHandler {
	return &BaseHandler{Name: name}
}

// ID for the action taken from the Name.
func (b *BaseHandler) ID() string {
	n := "Unnamed"

	if b != nil && b.Name != "" {
		n = b.Name
	}

	return n + " Action"
}

// Init the action. Init does nothing.
func (*BaseHandler) Init() error {
	return nil
}
