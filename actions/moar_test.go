package actions_test

import (
	"testing"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/store"
)

func TestMoar_Init(t *testing.T) {
	t.Run("Init should correctly handle the stored data", func(t *testing.T) {
		s := store.JSONStore{BackingStore: &store.Memory{}}

		a := actions.Moar{
			BackingStore: s,
			BaseHandler:  actions.NewBaseHandler("Moar"),
		}

		_ = s.Set("moar", a.Counter)

		if err := a.Init(); err != nil {
			t.Errorf("Unexpected error initialising Moar action: %v", err)
		}
	})
}
