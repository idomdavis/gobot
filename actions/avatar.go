package actions

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/bwmarrin/discordgo"
)

// Avatar will attempt to set the bot avatar.
func Avatar(avatar string, session *discordgo.Session) error {
	if avatar == "" {
		return nil
	}

	//nolint:gosec
	img, err := ioutil.ReadFile(avatar)

	if err != nil {
		return fmt.Errorf("failed to read avatar: %w", err)
	}

	contentType := http.DetectContentType(img)
	base64img := base64.StdEncoding.EncodeToString(img)
	payload := fmt.Sprintf("data:%s;base64,%s", contentType, base64img)

	_, err = session.UserUpdate("", "", "", payload, "")

	if err != nil {
		return fmt.Errorf("failed to set avatar: %w", err)
	}

	return nil
}
