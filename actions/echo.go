package actions

import (
	"fmt"

	"bitbucket.org/idomdavis/gobot/message"
	"github.com/sirupsen/logrus"
)

// Echo just displays what was sent to it.
type Echo struct {
	*BaseHandler
}

// Handle the chat message by echoing back the message details.
func (e *Echo) Handle(inbound message.Inbound, sender message.Sender) {
	err := sender.Send(message.Outbound{
		Channel: inbound.Channel,
		Message: fmt.Sprintf("[%s] %s: %s", inbound.Channel,
			inbound.Author.ID, inbound.Content),
	})

	if err != nil {
		logrus.WithError(err).Error("Failed to echo message")
	}
}
