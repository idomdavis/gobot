package actions

import (
	"os"
	"strings"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/message"
	"github.com/sirupsen/logrus"
)

// Shutdown the bot with the given version ID.
type Shutdown struct {
	*BaseHandler
}

// Handle the chat message by shutting down the bot.
func (s *Shutdown) Handle(inbound message.Inbound, sender message.Sender) {
	if !strings.HasPrefix(inbound.Content, "shutdown") {
		return
	}

	v := strings.TrimSpace(strings.TrimPrefix(inbound.Content, "shutdown"))

	if v != config.BuildVersion {
		return
	}

	if inbound.Author.ID != "192742534022168576" {
		return
	}

	err := sender.Send(message.Outbound{
		Channel: inbound.Channel,
		Message: "Dave, what are you doing Dave?",
	})

	if err != nil {
		logrus.WithError(err).Error("Failed to shutdown message")
	}

	os.Exit(0)
}
