package actions_test

import (
	"strings"
	"testing"

	"bitbucket.org/idomdavis/gobot/actions"
	"bitbucket.org/idomdavis/gobot/listener"
	"bitbucket.org/idomdavis/gobot/message"
)

func ExamplePing_Handle() {
	a := actions.Ping{}

	msg := message.Inbound{
		Channel: "chan",
		Content: "ping",
		Author:  message.User{ID: "user"},
	}

	s := listener.Console{}

	a.Handle(msg, s)

	// Output:
	// Pong!
}

func TestPing_Handle(t *testing.T) {
	t.Run("Action will respond to pong", func(t *testing.T) {
		p := actions.Ping{}

		msg := message.Inbound{
			Channel: "chan",
			Content: "pong",
			Author:  message.User{ID: "user"},
		}

		s := &message.MemorySender{}

		p.Handle(msg, s)

		switch {
		case len(s.Messages) != 1:
			t.Error("Expected a single response from pong")
		case !strings.HasPrefix(s.Messages[0].Message, "Let us"):
			t.Errorf("Unexpected response from pong: %s", s.Messages[0])
		}
	})

	t.Run("Action will ignore irrelevant text", func(t *testing.T) {
		p := actions.Ping{}

		msg := message.Inbound{
			Channel: "chan",
			Content: "let us play pong!",
			Author:  message.User{ID: "user"},
		}

		s := &message.MemorySender{}

		p.Handle(msg, s)

		if len(s.Messages) != 0 {
			t.Errorf("Unexpected response: %s", s.Messages)
		}
	})
}
