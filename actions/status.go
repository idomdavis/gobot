package actions

import (
	"fmt"
	"strings"
	"time"

	"bitbucket.org/idomdavis/gobot/config"
	"bitbucket.org/idomdavis/gobot/message"
	"github.com/sirupsen/logrus"
)

// Status just displays information about the bot.
type Status struct {
	Settings config.Settings

	*BaseHandler

	startTime time.Time
}

// Handle the chat message by replying with version information.
func (s *Status) Handle(inbound message.Inbound, sender message.Sender) {
	switch strings.ToLower(inbound.Content) {
	case "status", "version":
		msg := fmt.Sprintf(
			"%s: %s (Built: %s) - Uptime: %v",
			s.Settings.Discord.Name,
			config.BuildVersion,
			config.BuildTimestamp,
			time.Since(s.startTime),
		)

		if err := sender.SendString(inbound.Channel, msg); err != nil {
			logrus.WithError(err).Error("Failed send moar message")
		}
	default:
		return
	}
}

// Init the action.
func (s *Status) Init() error {
	s.startTime = time.Now()

	return nil
}
