// Package actions contains the actions that the bot can perform. Actions will
// be passed the current chat details and can return a response. More than one
// action can generate a response. Errors will result in no response from the
// action.
package actions
